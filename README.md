# Hiro - Responsive Flat Portfolio Template
A Fully Responsive flat and minimalist Portfolio HTML5/CSS3 template with 9 color schemes. Hiro can be used as a personal blog, photography, personal portfolio.

## Features
* **Clean & Flat Design**
* **Mobile-first Responsive Layout**
* **SEO Optimized**
* **Valid HTML5 / CSS3**
* **CSS3 Animations**
* **Lightweight & Bloat-free**
* **9 Color Schemes**
* **Commented Code**
* **Documentation Included**

## Browser Support
  * **IE9+**
  * **Google Chrome**
  * **Mozila Firefox**
  * **Safari**

## Pages Included

* **Home Page**
* **About Page**
* **Blog Page**
* **Contact Page**
* **Style Guide Page**


## Tags

portfolio, blog, clean, ipad, iphone, mobile, personal, responsive, retina, showcase, v-card, flat, minimal, photography, vertical menu, vertical navigation, full width
