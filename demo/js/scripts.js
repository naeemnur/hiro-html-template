/* =============================================================
		Custom Functions
 * ============================================================= */
$(function() {

    "use strict";

    // mobile menu
    $("<select />").appendTo("#access");
    $("<option />", {
        "selected": "selected",
        "class": "hamburger",
        "value": "",
        "text": "☰ Main Menu"
    }).appendTo("#access select");

    $("#access > ul > li:not([data-toggle])").each(function() {
        var el = $(this);
        var hasChildren = el.find("ul"),
            children = el.find("li > a");
        if (hasChildren.length) {
            $("<optgroup />", {
                "label": el.find("> a").text()
            }).appendTo("#access select");
            children.each(function() {
                $("<option />", {
                    "value": $(this).attr("href"),
                    "text": " - " + $(this).text()
                }).appendTo("optgroup:last");
            });
        } else {
            $("<option />", {
                "value": el.find("> a").attr("href"),
                "text": el.find("> a").text()
            }).appendTo("#access select");
        }
    });

    $("#access select").change(function() {
        window.location = $(this).find("option:selected").val();
    });

    // search overlay
    $('.search-btn').click(function() {
        $('.search-overlay').fadeIn();
    });
    $('.search-close').click(function() {
        $('.search-overlay').fadeOut();
    });


    //smooth scroll
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 300);
                return false;
            }
        }
    });

    // color switcher
    var selector = 1;
    $('#show-panel').click(function() {
        if (selector == 1) {
            $('.colors-switcher').animate({
                right: 0
            });
            $('#show-panel').animate({
                right: 170
            });
            selector = 0;
        } else {
            $('.colors-switcher').animate({
                right: -170
            });
            $('#show-panel').animate({
                right: 0
            });
            selector = 1;
        }
    });
});

// Nice Scroll
$("html").niceScroll();

// Testimonial Slider
(function() {
    'use strict';
    if ('querySelector' in document && 'addEventListener' in window && Array.prototype.forEach) {
        // Slider Variable
        var sliders = document.querySelectorAll('.slider');
        // If a Slider exists
        if (sliders) {
            [].forEach.call(sliders, function(slider) {
                var sliderID = slider.getAttribute('data-slider');
                // Activate Slider
                window[sliderID] = Swipe(slider, {
                    // Configure your options
                    speed: 500,
                    //auto: 5000,
                    continuous: true
                });
                // Function to display slide count
                var countSlides = function() {
                        // Variables
                        var slideTotal = window[sliderID].getNumSlides();
                        var slideCurrent = window[sliderID].getPos();
                        var slideCount = document.querySelector('#slide-count-' + sliderID);
                        // Content
                        if (slideCount) {
                            slideCount.innerHTML = slideCurrent + ' of ' + slideTotal;
                        }
                    }
                    // Run slide count function on load
                countSlides();
                // Create Previous & Next Buttons
                var slideNav = document.querySelector('#slide-nav-' + sliderID)
                if (slideNav) {
                    slideNav.innerHTML = '<a href="#" id="slide-nav-prev-' + sliderID + '"><i class="fa fa-arrow-left"></i></a> <a href="#" id="slide-nav-next-' + sliderID + '"><i class="fa fa-arrow-right"></i></i></a>';
                }
                // Toggle Previous & Next Buttons
                var btnNext = document.querySelector('#slide-nav-next-' + sliderID);
                var btnPrev = document.querySelector('#slide-nav-prev-' + sliderID);
                if (btnNext) {
                    btnNext.addEventListener('click', function(e) {
                        e.preventDefault();
                        window[sliderID].next();
                    }, false);
                }
                if (btnPrev) {
                    btnPrev.addEventListener('click', function(e) {
                        e.preventDefault();
                        window[sliderID].prev();
                    }, false);
                }
                // Toggle Left & Right Keypress
                window.addEventListener('keydown', function(e) {
                    if (e.keyCode == 37) {
                        window[sliderID].prev();
                    }
                    if (e.keyCode == 39) {
                        window[sliderID].next();
                    }
                }, false);
            });
        }
    }
    // Mix it up
    $('.projects').mixItUp();
})();