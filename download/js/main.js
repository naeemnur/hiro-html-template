/* =============================================================
		Custom Functions
 * ============================================================= */
$(function() {

	"use strict";

    // mobile menu
    $("<select />").appendTo("#access");
    $("<option />", {
        "selected": "selected",
        "class": "hamburger",
        "value": "",
        "text": "☰ Main Menu"
    }).appendTo("#access select");

    $("#access > ul > li:not([data-toggle])").each(function() {
        var el = $(this);
        var hasChildren = el.find("ul"),
            children = el.find("li > a");
        if (hasChildren.length) {
            $("<optgroup />", {
                "label": el.find("> a").text()
            }).appendTo("#access select");
            children.each(function() {
                $("<option />", {
                    "value": $(this).attr("href"),
                    "text": " - " + $(this).text()
                }).appendTo("optgroup:last");
            });
        } else {
            $("<option />", {
                "value": el.find("> a").attr("href"),
                "text": el.find("> a").text()
            }).appendTo("#access select");
        }
    });

    $("#access select").change(function() {
        window.location = $(this).find("option:selected").val();
    });

    // search overlay
    $('.search-btn').click(function() {
        $('.search-overlay').fadeIn();
    });
    $('.search-close').click(function() {
        $('.search-overlay').fadeOut();
    });


	//smooth scroll
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 300);
	        return false;
	      }
	    }
	  });
});

// Nice Scroll
$("html").niceScroll();